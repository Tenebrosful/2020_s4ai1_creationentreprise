document.write(`<!-- Footer -->
<footer class="page-footer font-small bg-nav pt-4">

    <!-- Footer Text -->
    <div class="container-fluid text-center text-md-left">

        <!-- Grid row -->
        <div class="row" id="footerRow">

            <!-- Grid column -->
            <div class="col-md-6 mt-md-0 mt-3 elemrow">

                <!-- Content -->
                <h5 class="text-uppercase font-weight-bold"><a href="./entreprise.html">Présentation de
                    l'entreprise</a></h5>
                <p>Présentation générale de l'entreprise, son histoire, son organisation, son objectif et ses
                    valeurs</p>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none pb-3 elemrow">

            <!-- Grid column -->
            <div class="col-md-6 mb-md-0 mb-3 elemrow">

                <!-- Content -->
                <h5 class="text-uppercase font-weight-bold"><a href="./produits.html">Nos produits et services</a>
                </h5>
                <p>Catalogue des différents produits et services proposés par DALAP</p>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none pb-3 elemrow">

            <!-- Grid column -->
            <div class="col-md-6 mb-md-0 mb-3 elemrow">

                <!-- Content -->
                <h5 class="text-uppercase font-weight-bold"><a href="./contact.html">Nous contacter</a></h5>
                <p>Envie de nous contacter ? Tu trouveras toutes les informations nécessaires sur cette page !</p>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Text -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2020 Pas Copyright:
        <a href="."> DALAP</a>
        <a data-target="#mentions" data-toggle="modal" href="#">| Mentions légales</a>
    </div>
    <!-- Copyright -->

    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="mentions" role="dialog"
         tabindex="-1">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-black-50" id="exampleModalLabel">Mentions légales</h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-black-50">
<h2>Généralités</h2>
<p>DALAP</p>
<p>SARL au capital de 1 900 €</p>
<p>Siège social : 1 Place du Général-de-Gaulle - 88000 Épinal - FRANCE</p>
<p>Contact :</p>
<ul>
<li>contact@DALAP.com</li>
<li>+33 6 29 89 10 63</li>
</ul>

<br><p>Associés :</p>
<ul>
<li>Hugo BERNARD</li>
<li>Paul CLAUDE</li>
<li>Raphaël FOLTZENLOGEL</li>
</ul>
<br><p>Site hébergé par la Direction Numérique de l’Université de Lorraine au travers de l'IUT Nancy-Charlemagne, l'une des composantes de l'Université de Lorraine.</p>


<br><h2>Traitements de données et non-responsabilité</h2>

<p>Ce site web n'effectue pas de traitement de données pour son propre compte.</p>

<p>Ce site web utilise des technologies externes pouvant effectuer des traitements de données personnelles. Pour exercer votre droit de retrait vis-à-vis de ces traitements, vous pouvez vous adresser aux compagnies en question :</p>
<ul>
<li>Google (utilisation de google maps et de google street view)</li>
</ul>

<br><p>Ce site web peut utiliser des liens hypertextes redirigeant vers d'autres sites. DALAP nie toute responsabilité vis-à-vis du contenu de ces sites.</p>

<h2>Accessibilité</h2>
<p>Les pages web ce de site ont été développées dans le respect des normes HTML 5.0 et CSS conformément aux spécifications édictées par le W3C.</p>
<p>Ce site web exploite des technologies telles que javascript dans le cadre de l'utilisation de BootStrap.</p>

<h2>Sources</h2>
<p>Iconographie :</p>
<p>Utilisation d'icône de freepik depuis le site 'flaticon'</p>
<p>https://www.flaticon.com/authors/freepik - www.flaticon.com</p>
                </div>
            </div>
        </div>
    </div>

</footer>
<!-- Footer -->`);