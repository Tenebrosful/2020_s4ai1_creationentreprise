document.write(`<nav class="navbar navbar-dark bg-nav" id="nav">
    <a class="navbar-brand" href=".">
        <img alt="" class="d-inline-block align-top" height="30" src="../img/logo.png" width="30">
        DALAP
    </a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="./entreprise.html">L'entreprise</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="./produits.html">Nos produits et services</a>
            </li>
        </ul>
    </div>
    <button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"
            class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button">
        <i class="fa fa-bars"></i>
    </button>
</nav>`);